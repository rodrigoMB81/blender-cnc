#escrito por Rodrigo Bellusci - 02/14
import bpy
import copy
import math

feedrateActual = 0
maxFeedrate = 100
unitsInMilimetes = True
unitsInInches = False
esperaPorDefecto = 200 #milisegundos
esperaActual = 0 #milisegundos
#listas globales
lista_de_renglones = []
lista_de_puntos = []

fresa = bpy.context.active_object

def feedrateASegundos(feedrate):
    return feedrate * (1.0/60.0)

def obtenerModulo(vector): #obtiene el modulo de un vector3 representado por una lista de 3 numeros
    resultado = math.sqrt(math.pow(vector[0], 2) + math.pow(vector[1], 2) + math.pow(vector[2], 2))
    return (copy.copy(resultado))

def obtenerDelta(puntoUno, puntoDos): #obtiene la resta entre dos vectores3 representados por listas
    resultado = [(puntoDos[0] - puntoUno[0]), (puntoDos[1] - puntoUno[1]), (puntoDos[2] - puntoUno[2])]
    return copy.copy(resultado)

def calcularCuadro(puntoInicial, puntoFinal, feed, espera = 0):
    if(feed != 0):
        feed = feedrateASegundos(feed)
        delta = obtenerDelta(puntoInicial, puntoFinal)
        distancia = obtenerModulo(delta)
        tiempo = distancia / feed
        proximoKeyframe = tiempo * 24 #24 es el numero de cuadros por segundo, depende de la config de la escena
        return math.ceil(proximoKeyframe)
    else:
        print("estoy por esperar:", espera)
        espera = espera / 1000.0
        proximoKeyframe = espera * 24
        return math.ceil(proximoKeyframe)

def cargarArchivo(camino):
    with open(camino) as archivo:
        for renglon in archivo:
            lista_de_renglones.append(renglon.strip().split(' '))

def insertarKeyFrame(ubicacion, cuadro):
    nuevo_cuadro = bpy.context.scene.frame_current + cuadro
    if(bpy.context.scene.frame_current < nuevo_cuadro): #si no hay suficientes cuadros, agregar los que falten
        bpy.context.scene.frame_end += nuevo_cuadro - bpy.context.scene.frame_end

    bpy.context.scene.frame_set(nuevo_cuadro)
    fresa.location = copy.copy(ubicacion)
    fresa.keyframe_insert(data_path="location", index=-1)

def interpretarGcode(un_renglon): #hay que arreglar esta funcion
    feedrateActual = 0
    punto = [0.0, 0.0, 0.0]
    for orden in un_renglon:
        if('F' in orden):
            feedrateActual = copy.copy(float(orden[1:]))
            print(orden[1:])
        if(orden == "G01" or orden == "G1"):
            copia_local = copy.copy(un_renglon)
            if(orden == "G01"):
                copia_local.remove("G01") #capaz que no haga falta hacer esto
            else:
                copia_local.remove("G1")
            for grupo in copia_local:
                if('X' in grupo):
                    punto[0] = float(grupo[1:])
                if('Y' in grupo):
                    punto[1] = float(grupo[1:])
                if('Z' in grupo):
                    punto[2] = float(grupo[1:])
            lista_de_puntos.append(copy.copy(punto))
            if(feedrateActual == 0):
                frame = calcularCuadro(fresa.location, punto, maxFeedrate)
            else:
                frame = calcularCuadro(fresa.location, punto, feedrateActual)
            insertarKeyFrame(punto, frame)
        if(orden == "G4" or orden == "G04"):
            for grupo in un_renglon:
                if('P' in grupo):
                    esperaActual = float(grupo[1:])
                    print("esperaActual es:", esperaActual)
                else:
                    esperaActual = esperaPorDefecto
                frame = calcularCuadro(fresa.location, fresa.location, 0, esperaActual)
                insertarKeyFrame(fresa.location, frame)


def crearCurva(curveName, objectName, tipo, lista): #crea una curva 3d
    w = 1 #peso del punto
    curvedata = bpy.data.curves.new(curveName, "CURVE")
    curvedata.dimensions = "3D"
    objectdata = bpy.data.objects.new(objectName, curvedata)
    objectdata.location = [0.0, 0.0, 0.0]
    bpy.context.scene.objects.link(objectdata)
    polyline = curvedata.splines.new(tipo)
    polyline.points.add(len(lista) - 1)
    for num in range (len(lista)):
        x, y, z = lista[num]
        polyline.points[num].co = (x, y, z, w)


def leerArchivo(lista):
    for renglon in lista:
        for orden in renglon:
            if('G' in orden):
                interpretarGcode(renglon)

cargarArchivo("C:\\Users\\Rodrigo\\Documents\\projects\\blender-cnc\\logo macgcode.nc")
leerArchivo(lista_de_renglones)
crearCurva("jojo", "jeje", "POLY", lista_de_puntos)