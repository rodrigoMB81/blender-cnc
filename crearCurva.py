#crea una curva a partir de una lista de puntos que se obtiene de un archivo de Gcode
import bpy
import copy

def crearCurva(curveName, objectName, type, lista): #crea una curva 3d
	w = 1 #peso del punto
	curvedata = bpy.data.curves.new(curveName, "CURVE")
	curvedata.dimensions = "3D"
	objectdata = bpy.data.objects.new(objectName, curvedata)
	objectdata.location = [0.0, 0.0, 0.0]
	bpy.context.scene.objects.link(objectdata)
	polyline = curvedata.splines.new(type)
	polyline.points.add(len(lista) - 1)
	for num in range (len(lista)):
		x, y, z = lista[num]
		polyline.points[num].co = (x, y, z, w)

puntos = []
lista_de_renglones = []
comando_encontrado = False
punto = [0.0, 0.0, 0.0]
w = 1
with open("C:\\Users\\Rodrigo\\Documents\\projects\\blender-cnc\\logo macgcode.nc") as archivo:
	for renglon in archivo:
		lista_de_renglones.append(renglon.strip().split(' '))

for renglon in lista_de_renglones:
	for orden in renglon:
		if('X' in orden):
			punto[0] = float(orden[1:])
		if('Y' in orden):
			punto[1] = float(orden[1:])
		if('Z' in orden):
			punto[2] = float(orden[1:])
	puntos.append(copy.copy(punto))

crearCurva("hola", "jelou", "POLY", puntos)