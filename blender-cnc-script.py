import serial
import time
import bpy

puertoSerial = serial.Serial('COM1', 9600, timeout=0)
time.sleep(1)

cube = bpy.data.objects['Cube']

coordenadas = [0.0, 0.0, 0.0]

while True :
	for i in range (0,3):
		coordenadas[i] = float(puertoSerial.readline())
	cube.location.x = coordenadas[i]