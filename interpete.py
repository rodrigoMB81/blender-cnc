import sys
#quita las G, X, Y, y Z de un archivo Gcode
escribir_en_archivo = False #Escribe el archivo parseado en un archivo nuevo llamado "nuevo_archivo"
escribir_a_coordenadas = False

archivo = open("logo macgcode.nc")

lista_de_lineas = []

orden = ""

coordenadas = [0.0, 0.0, 0.0]

for line in archivo:
	lista_de_ordenes = line.strip().split(' ') ##remueve el \n al final de cada linea y luego separa los elementos en los ' '
	lista_de_lineas.append(lista_de_ordenes) #recrea el archivo en una lista de lineas medianamente parseadas
	for grupo in lista_de_lineas:
		for orden in grupo:
			if('X' in orden): #remueve la X
				if(escribir_a_coordenadas == True):
					coordenadas[grupo.index(orden) - 1] = float(orden[1:])
				grupo[grupo.index(orden)] = orden[1:]
			if('Y' in orden): #remueve la Y
				if(escribir_a_coordenadas == True):
					coordenadas[grupo.index(orden) - 1] = float(orden[1:])
				grupo[grupo.index(orden)] = orden[1:]
			if('Z' in orden): #remueve la Z
				if(escribir_a_coordenadas == True):
					coordenadas[grupo.index(orden) - 1] = float(orden[1:])
				grupo[grupo.index(orden)] = orden[1:]
		for orden in grupo: #remueve el codigo G; es necesario borrarlo?
			if('G' in orden):
				grupo.remove(orden)
		for orden in grupo: #remueve la F
			if('F' in orden):
				grupo.remove(orden)
#print(lista_de_lineas)
#print(coordenadas)

print(sys.argv)

with open("nuevo_gcode", "wt") as nuevo_archivo :
	for grupo in lista_de_lineas:
		for orden in grupo:
			nuevo_archivo.write(orden)
			nuevo_archivo.write(' ')
		nuevo_archivo.write('\n')