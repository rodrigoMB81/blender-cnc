#anima un cubo en blender para que siga un camino generado a partir de un archivo de gcode
import bpy
cube = bpy.context.active_object


lista_de_lineas = []
orden = ""
posicion_nueva = cube.location
orden_encontrada = False
dibujar_linea = False
cuadro = 0
contador = 0
entre_cuadros = 1

#abro el archivo y guardo todos los renglones en una lista
with open("C:\\Users\\Rodrigo\\Documents\\projects\\blender-cnc\\logo macgcode.nc") as archivo:
	for renglon in archivo:
		lista_de_lineas.append(renglon.strip().split(' ')) #le quita a los renglones los espacios entre comandos y los separa
		#esto significa que cada renglo en lista_de_lineas es una lista

for renglon in lista_de_lineas: #hace que haya la cantidad de cuadros necesarios para animar, o mas
	contador += 1
	bpy.data.scenes["Scene"].frame_end = contador * entre_cuadros

for renglon in lista_de_lineas:
	bpy.context.scene.frame_set(cuadro)
	for orden in renglon:
		if('X' in orden):
			cube.location.x = float(orden[1:]) #asigna la posicion x
			if(orden_encontrada == False):
				orden_encontrada = True
		if('Y' in orden):
			cube.location.y = float(orden[1:]) #asigna la posicion y
			if(orden_encontrada == False):
				orden_encontrada = True
		if('Z' in orden):
			cube.location.z = float(orden[1:]) #asigna la posicion en z
			if(orden_encontrada == False):
				orden_encontrada = True
	if(orden_encontrada == True):
		cuadro += entre_cuadros
		#bpy.ops.anim.keyframe_insert(type='Location', confirm_success=True)
		cube.keyframe_insert(data_path="location", index=-1)
		orden_encontrada = False

bpy.ops.screen.frame_jump(end=False)
#bpy.ops.screen.animation_play()